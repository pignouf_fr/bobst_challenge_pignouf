# Bobst_Challenge_Pignouf

Control a bobst machine mockup with eye tracking and gestures using Leap motion

Members :
- Mathias DELAHAYE
- Thibault PORSSUT
- Francesca GIERUC
- Guillaume BROGGI

Machine simulator link:
https://team-bes-player-lauzhack-bobst.azurewebsites.net/
